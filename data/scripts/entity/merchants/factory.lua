package.path = package.path .. ";data/scripts/lib/ConfigLib/?.lua"

local ConfigLib = include("ConfigLib")
local ChangeMaxCargoConfigLib = ConfigLib.initialize("1723087726") -- Mod ID is configured in modinfo.lua


local MaxCargoBuildConfigUI = Factory.buildConfigUI
function Factory.buildConfigUI(tab)
	MaxCargoBuildConfigUI(tab)
	
    local maxStockSplit = UIVerticalSplitter(Rect(10 , tab.height - 38, tab.width, tab.height), 0, 0, 1 / 3)
	local maxStockFrame = maxStockSplit.right
	
	local maxStockVsplit = UIVerticalSplitter(maxStockFrame, 15, 0, 0.5)
	
	maxStockTextbox = tab:createTextBox(maxStockVsplit.left, "")
	maxStockTextbox.text = Entity():getValue("ham_max_stock") or ChangeMaxCargoConfigLib.get("globalMaxCargo")
	
	changeMaxStockButton = tab:createButton(maxStockVsplit.right, "Change max stock"%_t, "onChangeMaxStockButton")
end



function Factory.onChangeMaxStockButton(maxStock)
	if onClient() then
        invokeServerFunction("onChangeMaxStockButton", maxStockTextbox.text)
        return
    end
	
	local buyer, _, player = getInteractingFaction(callingPlayer, AlliancePrivilege.SpendResources, AlliancePrivilege.ManageStations)
    if not buyer then return end
	
	Entity():setValue("ham_max_stock", maxStock)
end
callable(Factory, "onChangeMaxStockButton")


-- special version of getMaxStock for factories
if GameVersion() >= Version(2, 0, 0) then -- Backwards compatibility for Avorion 1.*
	local CMCGetMaxStock = Factory.trader.getMaxStock
	Factory.trader.getMaxStock = function(self, good)
		-- Calculate ratio - stored in requiredSpaceRatioByGood
		CMCGetMaxStock(self, good)
		
		local ratio = requiredSpaceRatioByGood[good.name]
		if not ratio then
			return 0
		end

		local entity = Entity()
		local maxStock = entity.maxCargoSpace * ratio / good.size
		if maxStock > 100 then
			-- round to 100
			return math.min(entity:getValue("ham_max_stock") or ChangeMaxCargoConfigLib.get("globalMaxCargo"), round(maxStock / 100) * 100)
		else
			-- not very much space already, don't round
			return math.floor(maxStock)
		end
	end
end