package.path = package.path .. ";data/scripts/lib/ConfigLib/?.lua"
local ConfigLib = include("ConfigLib")
ChangeMaxCargoConfigLib = ConfigLib("1723087726") -- Mod ID is configured in modinfo.lua



-- Avorion 2: factory.lua got its own implementation of trader:getMaxStock()
-- Do we still need this one in future?
function TradingManager:getMaxStock(good)
    local entity = Entity()

    local space = entity.maxCargoSpace
    local slots = self.numBought + self.numSold

    if slots > 0 then space = space / slots end

    local goodSize = good.size
    if space / goodSize > 100 then
        -- round to 100
		return math.min(entity:getValue("ham_max_stock") or ChangeMaxCargoConfigLib.get("globalMaxCargo"), round(space / goodSize / 100) * 100)
    else
        -- not very much space already, don't round
        return math.floor(space / goodSize)
    end
end


