# Change Max Cargo

This little mod allows the player to change the max cargo size of his own stations. It will still be capped by the size of the cargo bay, but absolute cap of 25000 per good can be changed for every station. This is only possible for stations the player owns, AI stations are not affected.

The server admin (also a single player) can also change the absolute cap of 25000 to another value. It affects player- and npc stations. Custom settings for player stations as described above will overwrite this.

## Installation instructions
Subscribe to mod in Steam and enable it in your Avorion config.