# Changelog
### 0.3.1
* Remove backwards compatibility Avorion 1.* or below
* Fix a possible type issue in tradingmanager

### 1.3.0
* Updated mod for Avorion 2.*

### 1.2.1
* Updated mod for Avorion 1.0

### 1.2.0
* Updated mod for Avorion 0.29
* Improved code for better forward compatibility

### 1.1.0
* Updated mod to support ConfigLib 0.3.x and higher

### 1.0.0
* Update mod to support workshop
* Replace custom config by ConfigLib mod

### 0.2.0
* Provide compatibility to Avorion 0.21.x

### 0.1.4
* Provide compatibility to Avorion 0.20.2

### 0.1.3
* Provide compatibility to Avorion 0.19.1
* Fixed possible exploits inherited from vanilla game using negative trading prices
* Added new config to disable bought goods simulation on restoring from disk

### 0.1.2
* Updated for Avorion 0.17.1 r11753

### 0.1.1

* Updated for Avorion version 0.17.1

### 0.1.0

* Initial release